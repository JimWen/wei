// Wei.h : main header file for the WEI application
//

#if !defined(AFX_WEI_H__4FFE398E_C8CF_40CC_BBA2_8A03BEF9FABD__INCLUDED_)
#define AFX_WEI_H__4FFE398E_C8CF_40CC_BBA2_8A03BEF9FABD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CWeiApp:
// See Wei.cpp for the implementation of this class
//

class CWeiApp : public CWinApp
{
public:
	CWeiApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWeiApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CWeiApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WEI_H__4FFE398E_C8CF_40CC_BBA2_8A03BEF9FABD__INCLUDED_)
