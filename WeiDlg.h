// WeiDlg.h : header file
//

#if !defined(AFX_WEIDLG_H__1B5808EC_D610_4D4A_AB3B_0D579938B431__INCLUDED_)
#define AFX_WEIDLG_H__1B5808EC_D610_4D4A_AB3B_0D579938B431__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CWeiDlg dialog

#include "pmacruntime.h"

class CWeiDlg : public CDialog
{
// Construction
public:
	CWeiDlg(CWnd* pParent = NULL);	// standard constructor
	void PmacSetPInt(char *pStr, int p);

	//p1,p2分别代表速度1，速度2
// Dialog Data
	//{{AFX_DATA(CWeiDlg)
	enum { IDD = IDD_WEI_DIALOG };
	int		p1;
	int		p2;
	int		p3;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWeiDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CWeiDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnHome();
	afx_msg void OnMove();
	afx_msg void OnStop();
	afx_msg void OnBack();
	afx_msg void OnCirclemove();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WEIDLG_H__1B5808EC_D610_4D4A_AB3B_0D579938B431__INCLUDED_)
